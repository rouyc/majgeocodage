﻿using CbDll.Tools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace MajGeocodage
{
    public partial class ProClients
    {
        public string CtNum { get; set; }
        public byte[] AdresseHash { get; set; }
        public byte[] HASH_ADDRESS_INV { get; set; }
        public string CtAdresse { get; set; }
        public string CtComplement { get; set; }
        public string CtCodepostal { get; set; }
        public string CtVille { get; set; }
        public string CtCoderegion { get; set; }
        public string CtPays { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string ConnexionString { get; set; }

        public Task<ICollection<ProClients>> GetCreateOrUpdate()
        {
            ICollection<ProClients> sageProClients = new List<ProClients>();

            SQLTools.connexionString = this.ConnexionString;
            DataTable dtSageData = SQLTools.getSQLData("SELECT c.CT_Num , HASHBYTES('SHA2_512', c.CT_Adresse + c.CT_Complement + c.CT_CodePostal + c.CT_Ville + c.CT_Pays) as addressHash, cc.HASH_ADDRESS_INV, c.CT_Adresse, c.CT_Complement, c.CT_CodePostal, c.CT_Ville, c.CT_CodeRegion, c.CT_Pays " +
            "FROM F_COMPTET c " +
            "LEFT JOIN CBO_F_COMPTET cc ON cc.CT_NUM = c.CT_Num " +
            "WHERE c.CT_Type = 0 " +
            "AND (HASHBYTES('SHA2_512', c.CT_Adresse + c.CT_Complement + c.CT_CodePostal + c.CT_Ville + c.CT_Pays) <> cc.HASH_ADDRESS_INV) " +
            "OR cc.HASH_ADDRESS_INV IS NULL " +
            "ORDER BY c.CT_Num ");

            if (dtSageData.Rows.Count > 0)
            {
                foreach (DataRow drSageData in dtSageData.Rows)
                {
                    ProClients sageProClient = new ProClients
                    {
                        CtNum = drSageData["CT_Num"].ToString(),
                        AdresseHash = (byte[])drSageData["addressHash"],
                        CtAdresse = drSageData["CT_Adresse"].ToString(),
                        CtComplement = drSageData["CT_Complement"].ToString(),
                        CtCodepostal = drSageData["CT_CodePostal"].ToString(),
                        CtVille = drSageData["CT_Ville"].ToString(),
                        CtCoderegion = drSageData["CT_Coderegion"].ToString(),
                        CtPays = drSageData["CT_Pays"].ToString(),
                    };

                    if (!Convert.IsDBNull(drSageData["HASH_ADDRESS_INV"]))
                    {
                        sageProClient.HASH_ADDRESS_INV = (byte[])drSageData["HASH_ADDRESS_INV"];
                    }
                    sageProClients.Add(sageProClient);
                }
            }
            return Task.FromResult(sageProClients);
        }

        public Task<ICollection<ProClients>> GetDelete()
        {
            ICollection<ProClients> sageProClients = new List<ProClients>();

            SQLTools.connexionString = this.ConnexionString;
            DataTable dtSageData = SQLTools.getSQLData("SELECT cc.CT_NUM " +
            "FROM CBO_F_COMPTET cc " +
            "LEFT JOIN F_COMPTET c ON cc.CT_NUM = c.CT_Num " +
            "WHERE c.CT_Num IS NULL " +
            "ORDER BY cc.CT_Num ");

            if (dtSageData.Rows.Count > 0)
            {
                foreach (DataRow drSageData in dtSageData.Rows)
                {
                    ProClients sageProClient = new ProClients
                    {
                        CtNum = drSageData["CT_Num"].ToString(),
                    };
                    sageProClients.Add(sageProClient);
                }
            }
            return Task.FromResult(sageProClients);
        }
    }
}

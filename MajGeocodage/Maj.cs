﻿using CbDll.Tools;
using log4net;
using MajGeocodage.Models;
using Microsoft.EntityFrameworkCore;
using QuickType;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MajGeocodage
{
    public partial class Maj
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public String ApiBaseUrl { get; set; }
        public String ApiPath { get; set; }
        public String ApiResource { get; set; }
        public String ApiFormat { get; set; }
        public String ApiKey { get; set; }
        public String SageGCFile { get; set; }
        public String SageCPTAFile { get; set; }

        public async Task MajDataEF()
        {
            string ConnectionString = SageTools.getSQLServerConnexionString(this.SageGCFile);
            
            ProClients sageProClient = new ProClients
            {
                ConnexionString = ConnectionString
            };

            log.Debug("******* Récupération CreateOrUpdate *******");
            ICollection<ProClients> proClients2CreateOrUpdates = await sageProClient.GetCreateOrUpdate();
            DbContextOptionsBuilder<RACE_CIESQLContext> DbOptionsBuilder = new DbContextOptionsBuilder<RACE_CIESQLContext> ();
            DbOptionsBuilder.UseSqlServer(ConnectionString);
            foreach (ProClients proClient in proClients2CreateOrUpdates)
            {
                //Calcul du géocodage
                IRestResponse geocode = GetGeocodeAdresse(proClient);
                ResponseClass geocodeContent = Newtonsoft.Json.JsonConvert.DeserializeObject<ResponseClass>(geocode.Content);
                proClient.Latitude = geocodeContent?.Response?.View.FirstOrDefault()?.Result.FirstOrDefault()?.Location?.DisplayPosition?.Latitude ?? -1;
                proClient.Longitude = geocodeContent?.Response?.View.FirstOrDefault()?.Result.FirstOrDefault()?.Location?.DisplayPosition?.Longitude ?? -1;

                using (RACE_CIESQLContext db = new RACE_CIESQLContext(DbOptionsBuilder.Options))
                {
                    var data = new CboFComptet
                    {
                        CtNum = proClient.CtNum,
                        HashAddressInv = proClient.AdresseHash,
                        AddressInvLong = proClient.Longitude,
                        AddressInvLat = proClient.Latitude,
                    };

                    if (proClient.HASH_ADDRESS_INV == null)
                    {
                        //Si c'est null, c'est un nouveau alors on fait une insertion
                        log.Debug("Create " + proClient.CtNum);
                        db.Add(data);
                    }
                    else
                    {
                        //Sinon c'est qu'il existe déjà alors on fait une update
                        log.Debug("Update " + proClient.CtNum);
                        db.Update(data);
                    }
                    db.SaveChanges();
                }
            }

           log.Debug("******* Récupération Delete *******");

            ICollection<ProClients> sageDeletes = await sageProClient.GetDelete();
            foreach (ProClients sageDelete in sageDeletes)
            {
                log.Debug("Delete " + sageDelete.CtNum);
                using (RACE_CIESQLContext db = new RACE_CIESQLContext(DbOptionsBuilder.Options))
                {
                    var data = new CboFComptet 
                    { 
                        CtNum = sageDelete.CtNum 
                    };
                    db.Remove(data);
                    db.SaveChanges();
                }
            }
        }

        public IRestResponse GetGeocodeAdresse(ProClients clients)
        {
            IRestResponse restResponse = null;
            try
            {
                RestClient restClient = new RestClient(this.ApiBaseUrl + this.ApiPath);
                string adresse = this.ApiResource + this.ApiFormat + this.ApiKey + "&searchtext=" + clients.CtAdresse + "+" + clients.CtComplement + "+" + clients.CtCodepostal + "+" + clients.CtVille + "+" + clients.CtPays;
                RestRequest restRequest = new RestRequest(adresse, Method.GET);

                restResponse = restClient.Execute(restRequest);

            }
            catch (Exception ex)
            {
                log.Error("Erreur lors de la récupération du Géocodage" + ex);
            }
            return restResponse;
        }
    }
}

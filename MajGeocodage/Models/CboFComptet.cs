﻿using System;
using System.Collections.Generic;

namespace MajGeocodage.Models
{
    public partial class CboFComptet
    {
        public string CtNum { get; set; }
        public bool FtpSendInvoice { get; set; }
        public string FtpType { get; set; }
        public string FtpHostname { get; set; }
        public int FtpPort { get; set; }
        public string FtpUsername { get; set; }
        public string FtpPassword { get; set; }
        public string FtpSshKey { get; set; }
        public string FtpDirectory { get; set; }
        public double AddressInvLong { get; set; }
        public double AddressInvLat { get; set; }
        public byte[] HashAddressInv { get; set; }
    }
}

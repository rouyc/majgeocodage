﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace MajGeocodage.Models
{
    public partial class RACE_CIESQLContext : DbContext
    {
        public RACE_CIESQLContext()
        {
        }

        public RACE_CIESQLContext(DbContextOptions<RACE_CIESQLContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CboFComptet> CboFComptet { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=DESKTOP-JO4O73A;Database=RACE_CIESQL;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CboFComptet>(entity =>
            {
                entity.HasKey(e => e.CtNum);

                entity.ToTable("CBO_F_COMPTET");

                entity.Property(e => e.CtNum)
                    .HasColumnName("CT_NUM")
                    .HasMaxLength(17);

                entity.Property(e => e.AddressInvLat).HasColumnName("ADDRESS_INV_LAT");

                entity.Property(e => e.AddressInvLong).HasColumnName("ADDRESS_INV_LONG");

                entity.Property(e => e.FtpDirectory)
                    .HasColumnName("FTP_DIRECTORY")
                    .HasMaxLength(100);

                entity.Property(e => e.FtpHostname)
                    .HasColumnName("FTP_HOSTNAME")
                    .HasMaxLength(50);

                entity.Property(e => e.FtpPassword)
                    .HasColumnName("FTP_PASSWORD")
                    .HasMaxLength(20);

                entity.Property(e => e.FtpPort)
                    .HasColumnName("FTP_PORT")
                    .HasDefaultValueSql("((21))");

                entity.Property(e => e.FtpSendInvoice).HasColumnName("FTP_SEND_INVOICE");

                entity.Property(e => e.FtpSshKey)
                    .HasColumnName("FTP_SSH_KEY")
                    .HasMaxLength(100);

                entity.Property(e => e.FtpType)
                    .HasColumnName("FTP_TYPE")
                    .HasMaxLength(10)
                    .HasDefaultValueSql("(N'FTP')");

                entity.Property(e => e.FtpUsername)
                    .HasColumnName("FTP_USERNAME")
                    .HasMaxLength(20);

                entity.Property(e => e.HashAddressInv)
                    .IsRequired()
                    .HasColumnName("HASH_ADDRESS_INV")
                    .HasDefaultValueSql("((0))");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}

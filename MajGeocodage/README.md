Utiliser Entity Framework
===
#### 1/ Ajouter les package Nuget
---
- Microsoft.EntityFramework.Core.SqlServer
- Microsoft.EntityFramework.Core.Tools
- Microsoft.EntityFramework.Core.Proxies

#### 2/ Ajouter un dossier qui contiendra tous les fichiers g�n�r�s
---
#### 3/ Ouvrir la console du gestionnaire de package (Outils --> Gestionnaire de packages Nuget --> Console du gestionnaire de package) 
---
`Scaffold-DbContext "Server=RRR;Database=RRR;User Id= RRR; Password=RRR;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir NomDuDossier`

##### Si on utilise l�authentification Windows il faut remplacer User / Password par Trusted_Connection=True 
`Scaffold-DbContext "Server=DESKTOP-JO4O73A;Database=RACE_CIESQL;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir`

##### Pour limiter � une ou plusieurs tables il faut ajouter  -Tables � la fin de la commande -Tables NomDeLaTable
`Scaffold-DbContext "Server=DESKTOP-JO4O73A;Database=RACE_CIESQL;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir NomDuDossier -Tables NomDeLaTable`

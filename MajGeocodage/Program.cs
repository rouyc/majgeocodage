﻿using log4net;
using log4net.Config;
using Microsoft.Extensions.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace MajGeocodage
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
            MainAsync(args).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            log.Debug("******* MainAsync *******");
            //Récupération appsettings.json
            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");

            IConfiguration config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            Maj maj = new Maj
            {
                ApiBaseUrl = $"{config["ApiBaseUrl"]}",
                ApiPath = $"{config["ApiPath"]}",
                ApiResource = $"{config["ApiResource"]}",
                ApiFormat = $"{config["ApiFormat"]}",
                ApiKey = $"{config["ApiKey"]}",
                SageGCFile = $"{config["SageGCFile"]}",
                SageCPTAFile = $"{config["SageCPTAFile"]}"
            };

            await maj.MajDataEF();
        }
    }
}